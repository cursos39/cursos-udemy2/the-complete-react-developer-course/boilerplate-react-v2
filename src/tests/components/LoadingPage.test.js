import React from 'react'
import LoadingPage from '../../components/LoadingPage.js'
import { shallow } from 'enzyme'

test('should render LoadingPage correctly', () => {
  const wrapper = shallow(<LoadingPage />)
  expect(wrapper).toMatchSnapshot()
})
